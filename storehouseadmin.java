import java.math.*;
import java.security.*;
import java.util.*;

class StoreHouseAdmin implements Admin
{
	private DataBase root = new Category();
	private String username;
	private String password;
	private HashMap<Category,Category> productpath;
	private HashMap<Long, Category> productid;
	private static long id;
	
	public StoreHouseAdmin(DataBase d, String user, String pass)
	{
		root = d;
		id = 0;
		productid = new HashMap<Long, Category>();
		productpath = new HashMap<Category, Category>();
		this.setPassword(pass);
		this.setUsername(user);
	}
	public void InsertByPath(String path, String product)
	{
		String arr[] = path.split(" ");
		DataBase node = this.getRoot();
		ArrayList<Category> list = node.arr;
		String t;
		HashMap<Category, Category> ppath= this.getProductpath();
		HashMap<Long, Category> pid = this.getProductid();
		
		boolean flag ;
		
		for(int i=0;i<arr.length+1;i++)
		{
			if(i<arr.length)
				t = arr[i];
			else
				t = product;
			if(list.size()==0)
			{
				DataBase fresh = new Category(t);
				list.add((Category) fresh);
				node.setArr(list);
				list = fresh.arr;
				ppath.put((Category) fresh, (Category) node);
				pid.put(((Category)fresh).getId(), (Category) fresh);
				node = fresh;
			}
			else
			{
				flag = false;
				for(int j=0; j<list.size(); j++)
				{
					if(list.get(j).getType().equals(t))
					{
						flag = true;
						node = list.get(j);
						list = node.getArr();
						break;
					}
				}
				if(!flag)
				{
					DataBase fresh = new Category(t);
					list.add((Category) fresh);
					node.setArr(list);
					ppath.put((Category) fresh, (Category) node);
					pid.put(((Category)fresh).getId(), (Category) fresh);
					list = fresh.getArr();
					node = fresh;
				}
			}
		}
		this.setProductid(pid);
		this.setProductpath(ppath);
	}
	
	public void InsertById(String p, long c)
	{
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = pid.get(c);
		DataBase fresh = new Category(p);
		ArrayList<Category> list = obj.getArr();
		list.add((Category) fresh);
		pid.put(((Category)fresh).getId(), (Category) fresh);
		ppath.put((Category) fresh, obj);
	}
	public void ModifyById(double cost, int q, long c)
	{
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = pid.get(c);
		obj.setPrize(cost);
		obj.setQty(q);
	}
	public void DeleteById(long c)
	{
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = ppath.get(((pid.get(c))));
		ArrayList<Category> list = obj.getArr();
		list.remove(pid.get(c));
		obj.setArr(list);
	}
	public Category SearchByName(String n)
	{
		Category ans = null;
		ArrayList<Category> queue = (ArrayList<Category>) root.getArr().clone();
		while(!queue.isEmpty())
		{
			Category c = queue.get(0);
			queue.remove(0);
			if(c.getType().equals(n))
			{
				ans = c;
				return ans;
			}
			for(Category a : c.getArr())
			{
				if(a.getArr().equals(n))
				{
					ans = a;
					return ans;
				}
				queue.addAll(a.getArr());
			}
		}
		return ans;
	}
	public Category SearchById(long c)
	{
		HashMap<Long, Category> pid = this.getProductid();
		Category obj = null;
		obj = pid.get(c);
		return obj;
	}
	public static String MD5(String input) 
    { 
        try 
        { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) 
            { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) 
        { 
            throw new RuntimeException(e); 
        } 
    } 
	public boolean CheckForCredentials(String id, String pass) 
	{
		boolean ans = false;
		if(MD5(pass).equals(this.getPassword()) && id.equals(this.getUsername()))
		{
			ans = true;
		}
		return ans;
	}
	public DataBase getRoot() {
		return root;
	}
	public void setRoot(DataBase root) {
		this.root = root;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public HashMap<Category, Category> getProductpath() {
		return productpath;
	}
	public void setProductpath(HashMap<Category, Category> productpath) {
		this.productpath = productpath;
	}
	public HashMap<Long, Category> getProductid() {
		return productid;
	}
	public void setProductid(HashMap<Long, Category> productid) {
		this.productid = productid;
	}
}
public class storehouseadmin {

}
