import java.util.*;
class Category extends DataBase
{
	private String type;
	private long qty;
	private double prize;
	private long id;
	private static long code = 0;
	private static HashMap<String, Long> allproduct = new HashMap<String, Long>();
	
	
	public Category(String n, int num, double p)
	{
		super();
		if(allproduct.containsKey(n)==true)
		{
			this.setId(allproduct.get(n));
		}
		else
		{
			code++;
			this.setId(code);
			allproduct.put(n, code);
		}
		this.setType(n);
		this.setQty(num);
		this.setPrize(p);
	}
	public Category()
	{
		super();
		this.setId(code);
		this.setPrize(-1.0);
		this.setQty(-1);
		this.setType("");
	}
	public Category(String n)
	{
		super();
		if(allproduct.containsKey(n)==true)
		{
			this.setId(allproduct.get(n));
		}
		else
		{
			code++;
			this.setId(code);
			allproduct.put(n, code);
		}
		this.setId(code);
		this.setPrize(-1.0);
		this.setQty(-1);
		this.setType(n);
	}
	public String toString()
	{
		return(this.getType()+" " + this.getQty() + " " + this.getPrize()+" "+this.getId());
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getQty() {
		return qty;
	}
	public void setQty(long qty) {
		this.qty = qty;
	}
	public double getPrize() {
		return prize;
	}
	public void setPrize(double prize) {
		this.prize = prize;
	}
	public long getId() 
	{
		return id;
	}
	public void setId(long id) 
	{
		this.id = id;
	}
	public static long getCode() 
	{
		return code;
	}
	public static void setCode(long code) 
	{
		Category.code = code;
	}
}
public class category 
{

}


//e
//d
//d
//12
//500
//d
//e
//15
//600
//d
//f
//16
//700
//e