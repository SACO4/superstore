import java.util.*;
interface Admin
{
	public boolean CheckForCredentials(String id, String pass);
	public void InsertByPath(String path, String product);
	public void InsertById(String p, long c);
	public void ModifyById(double cost, int q, long c);
	public void DeleteById(long c);
	public Category SearchByName(String n);
	public Category SearchById(long c);
	
}
public class admin
{

}
