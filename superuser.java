import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
class SuperUser
{
	private ArrayList<StoreHouse> stores ;
	private ArrayList<WareHouse> warehouses ;
	private static long wareadminid;
	private static long storeadminid;
	private static long wareid ;
	private static long storeid ;
	private String username = "ds";
	private String password = "1234";
	public ArrayList<Category> PartialSearch(String part)
	{
		ArrayList<Category> ans = new ArrayList<Category>();
		for(WareHouse w : warehouses)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				String str = c.getType();
				if(str.contains(part))
				{
					ans.add(c);
				}
			}
		}
		for(StoreHouse w : stores)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				String str = c.getType();
				if(str.contains(part))
				{
					ans.add(c);
				}
			}
		}
		return ans;
		
	}
	public Category SearchByName(String n)
	{
		Category ans = null ;
		for(WareHouse w : warehouses)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getType().equals(n))
				{
					ans = c;
					return ans;
				}
			}
		}
		for(StoreHouse w : stores)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getType().equals(n))
				{
					ans = c;
					return ans;
				}
			}
		}
		return ans;
	}
	public Category SearchById(long id)
	{
		Category ans = null ;
		for(WareHouse w : warehouses)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getId()==id)
				{
					ans = c;
					return ans;
				}
			}
		}
		for(StoreHouse w : stores)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getId()==id)
				{
					ans = c;
					return ans;
				}
			}
		}
		return ans;
	}
	public SuperUser()
	{
		stores = new ArrayList<StoreHouse>();
		warehouses = new ArrayList<WareHouse>();
		wareid = 0;
		storeid = 0;
	}
	
	public String GenerateWareHouseAdminUsername()
	{
		String ans ;
		SuperUser.setWareadminid(SuperUser.getWareadminid()+1);
		ans = "WareHouse" + Long.toString(SuperUser.getWareid())+"@superstore.com";
		return ans;
	}
	public String GenerateWareHouseAdminPassword()
	{
		String ans ;
		ans = "WareHouse@" + Long.toString(SuperUser.getWareid());
		return ans;
	}
	public String GenerateStoreHouseAdminUsername()
	{
		String ans ;
		SuperUser.setStoreadminid(SuperUser.getStoreadminid()+1);
		ans = "StoreHouse" + Long.toString(SuperUser.getStoreid())+"@superstore.com";
		return ans;
	}
	public String GenerateStoreHouseAdminPassword()
	{
		String ans ;
		ans = "StoreHouse@" + Long.toString(SuperUser.getStoreid());
		return ans;
	}
	public long GenerateWareHouseId()
	{
		long ans ;
		SuperUser.setWareid(SuperUser.getWareid() + 1);
		ans = SuperUser.getWareid() ;
		return ans;
	}
	public long GenerateStoreHouseId()
	{
		long ans ;
		this.setStoreid(this.getStoreid() + 1);
		ans = this.storeid ;
		return ans;
	}
	public void CreateStoreHouse(String c, String s)
	{
		long id = GenerateStoreHouseId();
		StoreHouse fresh = new StoreHouse(id, c, s, GenerateStoreHouseAdminPassword(), GenerateStoreHouseAdminUsername());
		stores.add(fresh);
	}
	public void CreateWareHouse(String c, String s)
	{
		long id = GenerateWareHouseId();
		WareHouse fresh = new WareHouse(id, c, s, GenerateWareHouseAdminPassword(), GenerateWareHouseAdminUsername());
		System.out.println(fresh);
		warehouses.add(fresh);
	}
	public boolean CheckForCredentials(String id, String pass) 
	{
		boolean ans = false;
		if(MD5(pass).equals(MD5(this.getPassword())) && id.equals(this.getUsername()))
		{
			ans = true;
		}
		return ans;
	}
	public static String MD5(String input) 
    { 
        try 
        { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) 
            { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) 
        { 
            throw new RuntimeException(e); 
        } 
    }  
	public static long getWareid() {
		return wareid;
	}
	public static void setWareid(long wareid) {
		SuperUser.wareid = wareid;
	}
	public static long getStoreid() {
		return storeid;
	}
	public static void setStoreid(long storeid) {
		SuperUser.storeid = storeid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ArrayList<StoreHouse> getStores() {
		return stores;
	}
	public void setStores(ArrayList<StoreHouse> stores) {
		this.stores = stores;
	}
	public ArrayList<WareHouse> getWarehouses() {
		return warehouses;
	}
	public void setWarehouses(ArrayList<WareHouse> warehouses) {
		this.warehouses = warehouses;
	}
	public static long getWareadminid() {
		return wareadminid;
	}
	public static void setWareadminid(long wareadminid) {
		SuperUser.wareadminid = wareadminid;
	}
	public static long getStoreadminid() {
		return storeadminid;
	}
	public static void setStoreadminid(long storeadminid) {
		SuperUser.storeadminid = storeadminid;
	}
}
public class superuser 
{

}
